<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewsDetail;
use App\news_media;
use Validator;

class NewsController extends Controller
{
    public function homenews()
    {
        $data = NewsDetail::orderBy('created_at', 'desc')->take(10)->get();
        // dd($data);
        return view('home', compact('data'));
    }
    public function displaynews(Request $request)
    {
        $data = NewsDetail::orderBy('created_at', 'desc')->paginate(3);
        if ($request->ajax()) {
            $view = View::make('latestnewsajax', compact('data'));
            $previous_url = "";
            $next_url = "";
            if ($data->hasMorePages()) {
                $next_url = '<a href="' . $data->nextPageUrl() . '" id="next_link"><p>Next<img src="' . asset("images/generic/btn_arrow_r.png") . '" class="ml-2" alt="next"></p></a>';
            } else {
                $next_url = '<p>Next<img src="' . asset("images/frontend/generic/btn_arrow_r.png") . '" class="ml-2" alt="next"></p>';
            }
            if ($data->onFirstPage()) {
                $previous_url = '<p><img src="' . asset("images/frontend/generic/btn_arrow_l.png") . '" class="mr-2" alt="previous">Previous</p>';
            } else {
                $previous_url = '<a href="' . $data->previousPageUrl() . '" id="previous_link"><p><img src="' . asset("images/generic/btn_arrow_l.png") . '" class="mr-2" alt="previous">Previous</p></a>';
            }
            return response()->json(['html' => $view->render(), 'next_url' => $next_url, 'previous_url' => $previous_url, 'links' => $data->links('paginationlink')->toHtml()]);
        }
        return view('latestnews', compact('data'));       
    }
    public function detailnews($id)
    {
        $data = NewsDetail::where("id", $id)->first();
        return view('newsdetail', compact('data'));
    }
    public function listnews()
    {
        $data = NewsDetail::orderBy('created_at', 'desc')->paginate(15);
        return view('news/list', compact('data'));
    }
    public function loadinsertNews()
    {
        return view('news/insert');
    }
    public function insertNews(Request $request)
    {
        try {
            $element_array = array(
                'heading' => 'required',
                'date' => 'required'
            );
            $images = $request->file("allMediaFiles");
            $validator = Validator::make($request->all(), $element_array, [
                'heading.required' => 'Please enter heading',
                'date.required' => 'Please select date'
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            }
            $id = $request->input("id");
            if ($id == null) {
                $data = new NewsDetail();
            } else {
                $data = NewsDetail::where("id", $id)->first();
                if (!$data) {
                    return response()->json(['errors' => 'No news found']);
                }
            }
            $data->heading = $request->input("heading");
            $data->description = $request->input("description");
            $data->date = $request->input("date");
            $data->save();

            $deleted_ids = $request->input("deleted_ids");
            if ($deleted_ids != null && $id != null) {
                $deleted_ids_array = explode(",", $deleted_ids);
                foreach (count($deleted_ids_array) > 0 ? $deleted_ids_array : array() as $d) {
                    $file = news_media::where("id", $d)->where("newsId", $data->id)->first();
                    if ($file) {
                        if ($file->fileName != null && file_exists(public_path("uploads/" . $file->fileName))) {
                            unlink(public_path("uploads/" . $file->fileName));
                        }
                        $file->delete();
                    }
                }
            }
            if (count($images) > 0) {
                foreach ($images as $image) {
                    $news_file = new news_media();
                    $news_file->fileName = $image->getClientOriginalName();
                    $image->move(public_path() . '/uploads/', $news_file->fileName);
                    $news_file->newsId = $data->id;
                    $news_file->save();
                }
            }
            \Request::session()->flash('success', "News " . ($id == null ? "inserted" : "updated") . " successfully");
            return response()->json(array("list" => url("news")));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function editNews($id)
    {
        $data = NewsDetail::where("id", $id)->first();
        return view('news/insert', compact('data'));
    }

    public function deleteNews($id)
    {
        try {
            $data = NewsDetail::where("id", $id)->first();
            $data->delete();

            return redirect()->route("news")->with("success", "News deleted successfully");
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function deletemultiNews()
    {
        try {
            $ids_to_delete = request("news_ids");
            $news_ids = explode(",", $ids_to_delete);
            foreach (count($news_ids) > 0 ? $news_ids : array() as $n) {
                $data = NewsDetail::find($n);

                if ($data) {
                    $data->forceDelete();
                }
            }
            return "success";
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
