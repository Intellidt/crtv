<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class NewsDetail extends Model
{
    protected $table = 'news';
    // use SoftDeletes;
    function newsMedia()
    {
        return $this->hasMany('App\news_media', 'newsId', 'id');
    }
}
