<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout', 'Auth\LoginController@logout');

Route::get('/','NewsController@homenews');
Route::get('/aboutus', function () {
    return view('aboutus');
})->name('aboutus');
Route::get('/industrialarea', function () {
    return view('industrialarea');
})->name('industrialarea');
Route::get('/travelsection', function () {
    return view('travelsection');
})->name('travelsection');
Route::get('/movies', function () {
    return view('movies');
})->name('movies');
Route::get('/cinemaalliance', function () {
    return view('cinemaalliance');
})->name('cinemaalliance');
Route::get('/culturalfinance', function () {
    return view('culturalfinance');
})->name('culturalfinance');
Route::get('/fusionmedia', function () {
    return view('fusionmedia');
})->name('fusionmedia');
// Route::get('/latestnews', 'NewsController@displaynews')->name('latestnews');
Route::get('/latestnews', 'NewsController@displaynews')->name('latestnews');
Route::get('/newsdetail/{id}', 'NewsController@detailnews');
Route::get('/contactus', function () {
    return view('contactus');
})->name('contactus');
Route::get('/services', function () {
    return view('services');
})->name('services');

Route::group(['middleware' => ['auth']], function () {
/* NEWS */
Route::get('/news','NewsController@listnews')->name('news');
Route::get('/add-news','NewsController@loadinsertNews')->name('add-news');
Route::post('/add-news','NewsController@insertNews')->name('add-news');
Route::get('/edit-news/{id}', 'NewsController@editNews');
Route::get('/delete-news/{id}','NewsController@deleteNews');
Route::post('/delete-all-news', 'NewsController@deletemultiNews');
});
?>

