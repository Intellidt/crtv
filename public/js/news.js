var removed_ids = [];
var selected_files_array = {};
$(document).ready(function () {
    var date = new Date();
    $('#datepicker').datepicker({
        format: "yyyy-mm-dd"
    }).on('changeDate', function (e) {
        $(this).datepicker('hide');
         console.log("change :",$(this).datepicker("getDate"));
    });
    $('#datepicker').datepicker("setDate", new Date());
    $("#description").summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture']],
            ['view', ['codeview', 'help', 'undo', 'redo']],
        ],
        minHeight: 150,
        disableDragAndDrop: true
    });

    $("#media_uploader").unbind("change");
    $("#media_uploader").change(function (e) {
        var selected_files = this.files;
        var counter = 0;
        if ($("#media_container .file-preview").length > 0) {
            counter = parseInt($("#media_container .file-preview:last").attr("id").split("_")[1]);
        }
        counter = counter + 1;
        for (var s = 0; s < selected_files.length; s++) {
            var file, img;
            var _URL = window.URL || window.webkitURL;
            file = selected_files[s];

            img = new Image();
            img.onload = function () {
                var div = $("#file_preview_clone").clone();
                div.removeClass("invisible");
                div.removeAttr("id");
                var id = "div_" + counter;
                div.find(".file-preview-image").attr("src", this.src);
                div.attr("id", id);
                div.find(".remove").attr("onclick", "javascript:deleteFile('" + id + "')");
                $("#media_container").append(div);

                selected_files_array[counter] = this.file;
                counter++;
            };
            img.src = _URL.createObjectURL(file);
            img.file = file;
        }
    });
    $("#saveNews").unbind("click");
    $("#saveNews").click(function () {
        var data = new FormData();
        var formdata = $(".form-horizontal").serializeArray();
        for (var f in formdata) {           
            data.append(formdata[f].name, formdata[f].value);
        }
        for (var s in selected_files_array) {
            data.append("allMediaFiles[" + s + "]", selected_files_array[s]);
        }
        data.append("deleted_ids", removed_ids);
        $.ajax({
            url: $(".form-horizontal").data("url"),
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                $(".error").text("");                
                for (var e in data.errors) {
                    $(".error[for=" + e + "]").text(data.errors[e]);
                }
                 if (data.list != undefined) {
                     window.location.href = data.list;
                 }
            }
        });
    });
});

function deleteFile(div_id) {
    var file_index = div_id.split("_")[1];
    if (file_index != undefined) {
        if (selected_files_array[file_index] != undefined) {
            delete selected_files_array[file_index];
        } else {
            var existing_id = $("#" + div_id).find(".remove").data("id");
            if (existing_id != undefined) {
                removed_ids.push(existing_id);
            }
        }
        $("#" + div_id).remove();
    }
}
