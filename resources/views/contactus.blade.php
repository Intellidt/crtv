@extends('layout')
@section('content')
<div class="subpage">
    <!-- Banner -->
    <div class="banner mb-5" style="position:relative;">
            <img src="{{asset ('images/contact/banner_contact.jpg')}}" class="img-fluid" alt="contactbanner">
            <div class="w-100 smallcarouselcaption">
                    <h2 class="text-center">{{__('messages.contact_us')}}</h2>
            </div>
    </div>
    <div class="container">
        <div class="mb-3">
        <h4 class="text-center text-upeercase">WE'D LOVE TO HEAR FROM YOU </h4>
        </div>
        <div class="row mb-5">
            <div class="col-md-4">
                <img src="{{asset ('images/contact/icon_location.png')}}" class="img-fluid d-block m-auto contact_img" alt="location">
                <p class="text-center t1">China: {{__('messages.china_location')}}</p>
                <p class="text-center t1">Canada: {{__('messages.canada_location')}}</p>
            </div>
            <div class="col-md-4">
                <img src="{{asset ('images/contact/icon_phone.png')}}" class="img-fluid d-block m-auto contact_img" alt="phone">
                <p class="text-center t1">China: 52055271</p>
                <p class="text-center t1">Canada: (+1) (778) 297 7108</p>
            </div>
            <div class="col-md-4">
                <img src="{{asset ('images/contact/icon_mail.png')}}" class="img-fluid d-block m-auto contact_img" alt="mail">
                <p class="text-center t1">info@intellidt.com</p>
            </div>
        </div>
    </div>
</div>
@endsection
