@extends('layout')
@section('content')
<div class="subpage">
    <!-- Banner -->
    <div class="banner mb-5" style="position:relative;">
            <img src="{{asset ('images/business/banner_business.jpg')}}" class="img-fluid" alt="contactbanner">
            <div class="w-100 smallcarouselcaption">
                    <h2 class="text-center">{{__('messages.business_banner_text')}}</h2>
            </div>
    </div>
    <div class="container">
        <h3 class="text-center color-lightred">{{__('messages.business6_heading')}}</h3>
        <br>
        <p class="t1">{{__('messages.business6_detail')}}</p>
        <p class="t3 color-lightred">{{__('messages.business6_task1_heading')}}</p>
        <p class="t3 ">{{__('messages.business6_task1_detail')}}</p>
        <br>
        <p class="t3 color-lightred">{{__('messages.business6_task2_heading')}}</p>
        <p class="t3">{{__('messages.business6_task2_detail')}}</p>
    </div>
</div>
@endsection