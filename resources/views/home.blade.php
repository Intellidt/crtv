@extends('layout')
@section('content')
<div class="home">
    <!-- Banner -->
    <div class="banner mb-5" style="position:relative;">
        <img src="{{asset ('images/home/banner_home.jpg')}}" class="img-fluid" alt="homebanner">
        <div class="w-100 homecarouselcaption">
                <h2 class="text-center text-uppercase">{{__('messages.home_banner_text')}}</h2>
        </div>
    </div>
    <div class="below-banner container mb-5  ">
    <p class="text-justify t1 homebelowbanner">{{__('messages.home_below_banner')}}</p>
    </div>

    <!-- Six-Services -->
    <div class="mb-5">
    <div class="row container-fluid m-0">
        <div class="col-md-4 p-0 service">
            <img src="{{asset ('images/home/service_1.jpg')}}" class="img-fluid" alt="service1">
            <h1 class="service_content text-center text1">{{__('messages.service_1')}}</h1>
            <div class="overlay">
                <div class="text"><h1>{{__('messages.service_1')}}</h1>
                <p class="t1">{{__('messages.service_1_detail')}}</p>
            </div>
                <div class="m-3 readmore-border">
                    <a class="color-red" href="{!!route('services')!!}#service1"><p class="text-uppercase t1 readmore">Read More <i class="fas fa-plus-circle"></i></p>
                        </a>
                </div> 
            </div>
        </div>
        <div class="col-md-4 p-0 service">
            <img src="{{asset ('images/home/service_1.jpg')}}" class="img-fluid" alt="service2">
            <h1 class="service_content text-center text1">{{__('messages.service_2')}}</h1>
            <div class="overlay">
                <div class="text"><h1>{{__('messages.service_2')}}</h1>
                <p class="t1">{{__('messages.service_2_detail')}}</p>
                </div>
                <div class="m-3 readmore-border">
                    <a class="color-red" href="{!!route('services')!!}#service2"><p class="text-uppercase t1 readmore">Read More <i class="fas fa-plus-circle"></i></p></a>
                </div> 
            </div>
        </div>
        <div class="col-md-4 p-0 service">
            <img src="{{asset ('images/home/service_1.jpg')}}" class="img-fluid" alt="service3">
            <h1 class="service_content text-center text1">{{__('messages.service_3')}}</h1>
            <div class="overlay">
                <div class="text"><h1>{{__('messages.service_3')}}</h1>
                <p class="t1">{{__('messages.service_3_detail')}}</p>
                </div>
                <div class="m-3 readmore-border">
                    <a class="color-red" href="{!!route('services')!!}#service3"><p class="text-uppercase t1 readmore">Read More <i class="fas fa-plus-circle"></i></p></a>
                </div> 
            </div>
        </div>
            
    </div>
    <div class="row container-fluid m-0">
            <div class="col-md-4 p-0 service">
                    <img src="{{asset ('images/home/service_1.jpg')}}" class="img-fluid" alt="service4">
                    <h1 class="service_content text-center text1">{{__('messages.service_4')}}</h1>
                    <div class="overlay">
                        <div class="text"><h1>{{__('messages.service_4')}}</h1>
                        <p class="t1">{{__('messages.service_4_detail')}}</p>
                    </div>
                        <div class="m-3 readmore-border">
                            <a class="color-red" href="{!!route('services')!!}#service4"><p class="text-uppercase t1 readmore">Read More <i class="fas fa-plus-circle"></i></p></a>
                        </div> 
                    </div>
            </div>
            <div class="col-md-4 p-0 service">
                    <img src="{{asset ('images/home/service_1.jpg')}}" class="img-fluid" alt="service5">
                    <h1 class="service_content text-center text1">{{__('messages.service_5')}}</h1>
                    <div class="overlay">
                        <div class="text"><h1>{{__('messages.service_5')}}</h1>
                        <p class="t1">{{__('messages.service_5_detail')}}</p>
                        </div>
                        <div class="m-3 readmore-border">
                            <a class="color-red" href="{!!route('services')!!}#service5"><p class="text-uppercase t1 readmore">Read More <i class="fas fa-plus-circle"></i></p></a>
                        </div> 
                    </div>
            </div>
            <div class="col-md-4 p-0 service">
                    <img src="{{asset ('images/home/service_1.jpg')}}" class="img-fluid" alt="service6">
                    <h1 class="service_content text-center text1">{{__('messages.service_6')}}</h1>
                    <div class="overlay">
                        <div class="text"><h1>{{__('messages.service_6')}}</h1>
                        <p class="t1">{{__('messages.service_6_detail')}}</p>
                        </div>
                        <div class="m-3 readmore-border">
                            <a class="color-red" href="{!!route('services')!!}#service6"><p class="text-uppercase t1 readmore">Read More <i class="fas fa-plus-circle"></i></p></a>
                        </div> 
                    </div>
            </div>
    </div>
    </div>
    
   <!-- News-slider -->
   <div class="container mb-5 ">
        <h2 class="text-center">{{__('messages.latest_news')}}</h2>
    </div>
    <div class="container mb-5">
            <div class="row ml-xl-3 mr-xl-2 ml-lg-4 mr-lg-3 mx-md-4 mx-sm-3 mx-xs-3">
                    <div class="owl-carousel owl-theme" id="news-carousel">
                        @foreach($data as $d)
                            <div class="news-inner">
                                    @php $images=$d->newsMedia->first();@endphp
                                    @if(!empty($images))
                                    {{-- {{$images}} --}}
                                    <a href="{!!url('newsdetail/'.$d->id)!!}">
                                        <figure class="figure border border-grey news-height w-100 color-grey161">
                                            <div class="news_image">
                                                <img src="{{asset ('uploads/'.$images->fileName)}}" class="figure-img img-fluid w-100 m-auto " alt="{{$d->heading}}">
                                            </div>
                                            <p class="t1 text-justify mx-3 mt-3">{{$d->heading}}</p>
                                            <p class="t1 text-justify mx-3">{{date('d-m-Y', strtotime($d->date))}}</p>
                                        </figure>
                                    </a>
                        
                                    @else
                                    <a href="{!!url('newsdetail/'.$d->id)!!}">
                                        <figure class="figure border border-grey news-height w-100 color-grey161">
                                            <p class="t1 text-justify mx-3 mt-3">{{$d->heading}}</p>
                                            <p class="t1 text-justify mx-3">{{date('d-m-Y', strtotime($d->date))}}</p>
                                            <div class="t1 text-justify mx-3 news_description">
                                                {{str_limit(preg_replace('/<[^>]*>/', '', $d->description),200)}}
                                            </div>
                                        </figure>
                                    </a>
                                    @endif
                            </div>
                        @endforeach
                    </div>
            </div>
    </div>
</div>
<script src="{!! asset('js/owl.carousel.js') !!}"></script>
<script>
    $('#news-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<img src='{!! asset('images/generic/btn_arrow_l.png') !!}' alt='previous'>", "<img src='{!! asset('images/generic/btn_arrow_r.png') !!}' alt='next'>"],
        navElement: 'div',
        dots: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            576: {

                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    })
</script>
@endsection
