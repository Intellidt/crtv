@extends('layout')
@section('content')
<div class="subpage">
    <!-- Banner -->
    <div class="banner mb-5" style="position:relative;">
            <img src="{{asset ('images/service/banner_service.jpg')}}" class="img-fluid" alt="servicebanner">
            <div class="w-100 smallcarouselcaption">
                    <h2 class="text-center">Experience meets Expertise(replace)</h2>
            </div>
    </div>
    <div class="container">
        <div id="service1">
        <h4>{{__('messages.service_1')}}</h4>
        <hr class="bg-gray63 ">
        <div class="row mb-5 servicedetailimage">
                <div class="col-md-5">
                    <h3 class=" color-lightred text-left">{{__('messages.services_1_line')}}</h3>
                </div>
                <div class="col-md-7 ">
                    <p>{{__('messages.services_1_detail1')}}</p>
                    <p>{{__('messages.services_1_detail2')}}</p>
                    <p>{{__('messages.services_1_detail3')}}</p>
                </div>
            
                <div class="col-md-4">
                <img class=" d-block  img-fluid" src="{{asset('images/service/img_1.jpg')}}" alt="service">
                </div>
                <div class="col-md-4">
                <img class=" d-block  img-fluid" src="{{asset('images/service/img_2.jpg')}}" alt="service">
                </div>
            
        </div>
        </div>
        <div id="service2">
        <h4>{{__('messages.service_2')}}</h4>
        <hr class="bg-gray63 ">
        <div class="row mb-5 servicedetailimage">
                <div class="col-md-5">
                    <h3 class=" color-lightred text-left">{{__('messages.services_2_line')}}</h3>
                </div>
                <div class="col-md-7">
                    <p>{{__('messages.services_2_detail')}}</p>
                </div>
        </div>
        </div>
        <div id="service3">
        <h4>{{__('messages.service_3')}}</h4>
        <hr class="bg-gray63 ">
        <div class="row mb-5 servicedetailimage">
                <div class="col-md-5">
                    <h3 class="color-lightred text-left">{{__('messages.services_3_line')}}</h3>
                </div>
                <div class="col-md-7">
                    <p>{{__('messages.services_3_detail')}}</p>
                </div>
                <div class="col-md-4">
                    <img class=" d-block img-fluid" src="{{asset('images/service/img_1.jpg')}}"  alt="service">
                </div>
                <div class="col-md-4">
                    <img class=" d-block  img-fluid" src="{{asset('images/service/img_2.jpg')}}" alt="service">
                </div>
                <div class="col-md-4">
                    <img class=" d-block img-fluid" src="{{asset('images/service/img_2.jpg')}}" alt="service">
                </div>
        </div>
        </div>
        <div id="service4">
        <h4>{{__('messages.service_4')}}</h4>
        <hr class="bg-gray63 ">
        <div class="row mb-5 servicedetailimage">
                <div class="col-md-5">
                    <h3 class="color-lightred text-left">{{__('messages.services_4_line')}}</h3>
                </div>
                <div class="col-md-7">
                    <p>{{__('messages.services_4_detail')}}</p>
                </div>
                <div class="col-md-4">
                    <img class=" d-block img-fluid" src="{{asset('images/service/img_1.jpg')}}"  alt="service">
                </div>
                <div class="col-md-4">
                    <img class=" d-block img-fluid" src="{{asset('images/service/img_2.jpg')}}" alt="service">
                </div>
        </div>
        </div>
        <div id="service5">
        <h4>{{__('messages.service_5')}}</h4>
        <hr class="bg-gray63 ">
        <div class="row mb-5 servicedetailimage">
                <div class="col-md-5">
                    <h3 class="color-lightred text-left">{{__('messages.services_5_line1')}}</h3>
                    <h3 class="color-lightred text-left">{{__('messages.services_5_line2')}}</h3>
                </div>
                <div class="col-md-7">
                        <h4>{{__('messages.services_5_detail1_point')}}</h4>
                        <p>{{__('messages.services_5_detail1')}}</p>
                        <h4>{{__('messages.services_5_detail2_point')}}</h4>
                        <p>{{__('messages.services_5_detail2')}}</p>
                        <h4>{{__('messages.services_5_detail3_point')}}</h4>
                        <p>{{__('messages.services_5_detail3')}}</p>
                </div>
                <!-- ICONS -->
                <div class="d-flex flex-wrap m-auto justify-content-center">
                    <div class="p-2 bd-highlight">
                        <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                <img class="manImg" src="{{asset('images/service/icon_operation.png')}}" alt="icon">
                        </span>
                        <span>{{__('messages.icon_1_name')}}</span>
                    </div>
                    
                    <div class="p-2 bd-highlight">
                        <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                <img class="manImg" src="{{asset('images/service/icon_fans.png')}}" alt="icon">
                        </span>
                        <span>{{__('messages.icon_2_name')}}</span>
                    </div>

                    <div class="p-2 bd-highlight">
                        <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                <img class="manImg" src="{{asset('images/service/icon_talents.png')}}" alt="icon">
                        </span>
                        <span>{{__('messages.icon_3_name')}}</span>
                    </div>

                    <div class="p-2 bd-highlight">
                        <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                <img class="manImg" src="{{asset('images/service/icon_internet.png')}}" alt="icon">
                        </span>
                        <span>{{__('messages.icon_4_name')}}</span>
                    </div>

                    <div class="p-2 bd-highlight">
                        <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                <img class="manImg" src="{{asset('images/service/icon_finance.png')}}" alt="icon">
                        </span>
                        <span>{{__('messages.icon_5_name')}}</span>
                    </div>
                </div>

                <div class="d-flex flex-wrap m-auto justify-content-center">
                        <div class="p-2 bd-highlight">
                            <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                    <img class="manImg" src="{{asset('images/service/icon_event.png')}}" alt="icon">
                            </span>
                            <span>{{__('messages.icon_6_name')}}</span>
                        </div>
                        
                        <div class="p-2 bd-highlight">
                            <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                    <img class="manImg" src="{{asset('images/service/icon_ecommerce.png')}}" alt="icon">
                            </span>
                            <span>{{__('messages.icon_7_name')}}</span>
                        </div>
    
                        <div class="p-2 bd-highlight">
                            <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                    <img class="manImg" src="{{asset('images/service/icon_hygiene.png')}}" alt="icon">
                            </span>
                            <span>{{__('messages.icon_8_name')}}</span>
                        </div>
    
                        <div class="p-2 bd-highlight">
                            <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                    <img class="manImg" src="{{asset('images/service/icon_copyright.png')}}" alt="icon">
                            </span>
                            <span>{{__('messages.icon_9_name')}}</span>
                        </div>
    
                        <div class="p-2 bd-highlight">
                            <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
                                    <img class="manImg" src="{{asset('images/service/icon_ad.png')}}" alt="icon">
                            </span>
                            <span>{{__('messages.icon_10_name')}}</span>
                        </div>
                    </div>
        </div>
        </div>
        <div id="service6">
        <h4>{{__('messages.service_6')}}</h4>
        <hr class="bg-gray63 ">
        <div class="row mb-5 servicedetailimage">
                <div class="col-md-5">
                    <h3 class="color-lightred text-left">{{__('messages.services_6_line')}}</h3>
                </div>
                <div class="col-md-7">
                        <p>{{__('messages.services_6_detail1')}}</p>
                        <p>{{__('messages.services_6_detail2')}}</p>
                </div>
        </div>
        </div>
    </div>
</div>
@endsection