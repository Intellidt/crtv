@extends('layout')
@section('content')
<div class="subpage">
    <!-- Banner -->
    <div class="banner mb-5" style="position:relative;">
            <img src="{{asset ('images/contact/banner_contact.jpg')}}" class="img-fluid" alt="contactbanner">
            <div class="w-100 smallcarouselcaption">
                    <h2 class="text-center">{{__('messages.about_us')}}</h2>
            </div>
    </div>
    <div class="container">
    <!-- Company Profile -->  
        <p class="t3">{{__('messages.company_profile')}}</p>
        <hr class="bg-gray63 ">
        <div class="row mb-5">
            <div class="col-md-5">
                <h3 class="text-left">{{__('messages.company_profile_heading')}}</h3>
            </div>
            <div class="col-md-7">
                <p class="t1 text-justify">{{__('messages.company_profile_detail1')}}</p>
                <p class="t1 text-justify">{{__('messages.company_profile_detail2')}}</p>
            </div>
        </div>
    <!-- Our Team -->  
        <p class="t3">{{__('messages.our_team')}}</p>
        <hr class="bg-gray63 ">
        <div class="row mb-5">
            <div class="col-md-5">
                <h3 class="text-left">{{__('messages.our_team_heading')}}</h3>
            </div>
            <div class="col-md-7">
                <div id="member1">
                    <p><span class="t3 mr-3">{{__('messages.member1_name')}}</span></p>
                    <p class="t1 text-justify">{{__('messages.member1_detail')}}</p>
                </div>
                <br>
                <div id="member2">
                    <p><span class="t3 mr-3">{{__('messages.member2_name')}}</span></p>
                    <p class="t1 text-justify">{{__('messages.member2_detail')}}</p>
                </div>
                <br>
                <div id="member3">
                    <p><span class="t3 mr-3">{{__('messages.member3_name')}}</span></p>
                    <p class="t1 text-justify">{{__('messages.member3_detail')}}</p>
                </div>
                <br>
                <div id="member4">
                    <p><span class="t3 mr-3">{{__('messages.member4_name')}}</span></p>
                    <p class="t1 text-justify">{{__('messages.member4_detail')}}</p>
                </div>
                <br>
                <div id="member5">
                    <p><span class="t3 mr-3">{{__('messages.member5_name')}}</span></p>
                    <p class="t1 text-justify">{{__('messages.member5_detail')}}</p>
                </div>
               
            </div>
        </div>
</div>
@endsection