@extends('layout')
@section('content')
<div class="bg-white">
<div class="subpage container">
    <div class="row pb-5">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            @php
            $images=$data->newsMedia;
            // dd($images);
            @endphp
            @if(count($images)>0)
            <div class="mx-5">
                <div class="detail">
                <div id="topArticles" class="carousel slide my-5" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @php
                                $images=$data->newsMedia;
                                // dd($images);
                            @endphp
                            @foreach($images as $key=> $s)
                            @if(count($images) > 1)
                            <li data-target="#topArticles" data-slide-to="{{$key}}" class="{!!$key==0?" active ":" "!!}"></li>
                            @endif
                            @endforeach
                        
                        </ol>

                        <div class="carousel-inner m-auto" >
                                @php
                                $images=$data->newsMedia;
                                // dd($images);
                            @endphp
                            @foreach($images as $key=> $s)
                            <div class="carousel-item {!!$key==0 ?" active ":" " !!}" >
                                <img src="{{asset('uploads/'.$s->fileName)}}" class="d-block detail_image img-fluid" alt="{{ $s->heading }}" >
                            </div>
                            @endforeach
                        </div>
                </div>
                        <h2 class="text-center color-black">{{$data->heading}}</h2>
                        <p class="text-center mb-5 color-black">{{date('d-m-Y', strtotime($data->date))}}</p>
                       
                </div>
            </div>
            @else
            <div class="mx-5 mt-5">
                    <div class="detail">
                            <h2 class="text-center color-black">{{$data->heading}}</h2>
                            <p class="text-center mb-5 color-black">{{date('d-m-Y', strtotime($data->date))}}</p>
                            <div class="text-justify color-black mx-2">
                                {{preg_replace('/<[^>]*>/', '', $data->description)}}
                            </div>
                        
                    </div> 
            </div>
            @endif
        </div>
        <div class="col-md-2">
        </div>    
    </div>
</div>
</div>
@endsection