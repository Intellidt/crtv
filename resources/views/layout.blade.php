<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title>{{ config('app.name', 'CRTV') }}</title>
    
    <script src ="{{asset ('js/jquery.min.js')}}"></script>
    <script src ="{{asset ('js/bootstrap.min.js')}}"></script>
    <script src ="{{asset ('js/all.js')}}"></script>
    <link href="{{asset ('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset ('css/style.css')}}" rel="stylesheet">
    <link href="{{asset ('css/all.css')}}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.css') }}" rel="stylesheet">

    </head>
    <body>
    <div class="main-content-wrapper bg-black">
            <div class="container-fluid">
                    <div class="container" style="position: relative;">
                    <nav class="navbar navbar-expand-lg">
                        <a href="{!! url('/')!!}">
                            <img src="{{asset('images/logo.png')}}" alt="logo" class="logo">
                        </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="navbar-toggler-icon"></span>
                            </button>
                          
                            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                              <ul class="navbar-nav " >
                               
                                <li class="nav-item">
                                        <a class="nav-link" href="{!!route('aboutus')!!}">{{__('messages.about_us')}}</a> 
                                </li>
                                {{-- <li class="nav-item">
                                  <a class="nav-link" href="#">{{__('messages.business_introduction')}}</a>
                                </li> --}}
                                <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{__('messages.business_introduction')}}
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="{!!route('industrialarea')!!}" style="font-family:Playfair Display Regular;font-size:15pt;">{{__('messages.business1')}}</a>
                                        <a class="dropdown-item" href="{!!route('travelsection')!!}" style="font-family:Playfair Display Regular;font-size:15pt;">{{__('messages.business2')}}</a>
                                        <a class="dropdown-item" href="{!!route('movies')!!}" style="font-family:Playfair Display Regular;font-size:15pt;">{{__('messages.business3')}}</a>
                                        <a class="dropdown-item" href="{!!route('cinemaalliance')!!}" style="font-family:Playfair Display Regular;font-size:15pt;">{{__('messages.business4')}}</a>
                                        <a class="dropdown-item" href="{!!route('culturalfinance')!!}" style="font-family:Playfair Display Regular;font-size:15pt;">{{__('messages.business5')}}</a>
                                        <a class="dropdown-item" href="{!!route('fusionmedia')!!}" style="font-family:Playfair Display Regular;font-size:15pt;">{{__('messages.business6')}}</a>
                                        </div>
                                </li>
                                <li class="nav-item">
                                        <a class="nav-link" href="{!!route('latestnews')!!}">{{__('messages.latest_news')}}</a> 
                                </li>
                                <li class="nav-item">
                                        <a class="nav-link" href="{!!route('contactus')!!}">{{__('messages.contact_us')}}</a> 
                                </li>
                              </ul>
                            </div>
                    </nav>
                   </div>
                   <hr class="bg-red m-0 border-2">
                    
                    <main class="p-0">
                            @yield('content')
                    </main>
            </div>
    </div>
    <div class="container-fluid bg-black">
                    <!-- Footer -->
                    <hr class="bg-gray63 my-0">
                    <span class="footer-text d-block footer text-center py-3">© 2019 CRTV. All Right Reserved. Designed by Intelli Group
                    </span>
            </div>
    </body>
</html>