{{-- articles --}}
@if(count($data)!="")
<div class="row mb-3">
    <?php

    $numOfCols = 3;
    $rowCount = 0;
    $bootstrapColWidth = 12 / $numOfCols;
    $bootstrapColWidth1 = $bootstrapColWidth * 2;
    $bootstrapColWidth2 = 12;
    ?>

    <?php
    foreach ($data as $d) {
        ?>

        <div class="col-md-<?php echo $bootstrapColWidth; ?>">

            @php $images=$d->newsMedia->first();@endphp

            @if(!empty($images))
            {{-- {{$images}} --}}
            <a href="{!!url('newsdetail/'.$d->id)!!}">
                <figure class="figure border border-grey news-height w-100 color-grey161">
                    <div class="news_image">
                        <img src="{{asset ('uploads/'.$images->fileName)}}" class="figure-img img-fluid w-100 m-auto h-100" alt="{{$d->heading}}">
                    </div>
                    <p class="text-justify mx-3 mt-3">{{$d->heading}}</p>
                    <p class="text-justify mx-3">{{date('d-m-Y', strtotime($d->date))}}</p>
                </figure>
            </a>

            @else
            <a href="{!!url('newsdetail/'.$d->id)!!}">
                <figure class="figure border border-grey news-height w-100 color-grey161">
                    <p class="text-justify mx-3 mt-3">{{$d->heading}}</p>
                    <p class="text-justify mx-3">{{date('d-m-Y', strtotime($d->date))}}</p>
                    <div class="text-justify mx-3 news_description">
                        {{str_limit(preg_replace('/<[^>]*>/', '', $d->description),200)}}
                    </div>
                </figure>
            </a>
            @endif



        </div>
        <?php
        $rowCount++;
        if ($rowCount % $numOfCols == 0) echo '</div> <div class="row">';
    }
    ?>
</div>
@endif


