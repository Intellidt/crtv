@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
            <div class="alert alert-success" id="successMessage">
                {!! session('success') !!}
            </div>
            @endif @if(session()->has('error'))
            <div class="alert alert-danger" id="errorMessage">
                {!! session('error') !!}
            </div>
            @endif
        </div>
        <div class="news_list mx-2 my-4 text-right w-100">
            <button onclick="javascript:deleteSelectedNews();" class="btn btn-primary rounded p-2">Delete News</button>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped-col">
                <thead>
                    <tr>
                        <th data-column="checkbox"><input type="checkbox" id="delete_checkall" /></th>
                        <th>Heading</th>
                        <th>Image</th>
                        <th>Date</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <td><input type="checkbox" class="check_single" value="{{$d->id}}" /></td>
                        <td>{{ $d->heading }}</td>
                        <td>
                            @if($d->newsMedia->first())
                            @php
                            $news_media = $d->newsMedia->first();
                            @endphp
                            <img src="{!! asset('uploads').'/'.$news_media->fileName !!}" alt="{!! $d->heading !!}" class="img-thumbnail" width="200" />
                            @endif
                        </td>
                        <td>{{ date('Y-m-d', strtotime($d->date)) }}</td>
                        <td><a class="btn btn-primary" href="{{url('edit-news/'.$d->id)}}"><i class="fas fa-edit"></i></a></td>
                        {{-- <td><a href="{{url('delete-news/'.$d->id)}}"><i class="fa fa-trash-alt"></i></a></td> --}}
                        <td><a class="btn btn-danger" onclick="return confirm('Are you sure want to delete?')" href="{{url('delete-news/'.$d->id)}}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="float-right">
                {{ $data->links() }}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $("#successMessage,#errorMessage").delay(5000).slideUp(300);
    });

    $('#delete_checkall').click(function() {
        if ($(this).is(':checked')) {
            $('.check_single').prop('checked', true);
        } else {
            $('.check_single').prop('checked', false);
        }
    });

    function deleteSelectedNews() {
        var news_ids = "";
        var checkedVals = $(".check_single:checkbox:checked").map(function() {
            return this.value;
        }).get();
        news_ids = checkedVals.join(",");
        if (news_ids == "") {
            alert("Please select news");
            return false;
        } else {
            if (confirm("Are you sure want to delete news?")) {
                $.ajax({
                    type: 'POST',
                    url: '{!! url("delete-all-news") !!}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'news_ids': news_ids
                    },
                    success: function(data) {
                        // console.log(data);
                        if ($.trim(data) == "success") {
                            $(".news_list").before('<div class="alert alert-success" id="successMessage">News deleted successfully</div>');
                            $("#successMessage").delay(5000).slideUp(300);
                            window.location.reload();

                        }
                    }
                });
            }
        }
    }
</script>
@endsection