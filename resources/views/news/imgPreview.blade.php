<div class="col-md-3 mb-3 mr-3 file-preview text-center @if(!isset($file_name))invisible @endif" @if(!isset($file_name)) id="file_preview_clone" @else id="div_{!! $image_id !!}" @endif>
    <img @if(isset($file_name)) src="{{ asset('uploads/'.$file_name) }}" @endif class="file-preview-image mb-3 ">
    <div>
        <button class="btn btn-primary remove" type="button" @if(isset($image_id)) data-id="{!! $image_id !!}" onclick="javascript:deleteFile('div_{!! $image_id !!}')" @endif>Remove</button>
    </div>
</div>