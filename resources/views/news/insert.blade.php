@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Add news</div>
                <div class="card-body">
                    {!! Form::model((isset($data) ? $data : array()),['method'=>"POST",'data-url'=> url('add-news'),'files' => true,'class'=>'form-horizontal']) !!}
                    {!! Form::hidden("id",(isset($data) ? $data->id : "")) !!}
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {!! Form::label('heading','Heading <span class="text-danger">*</span>',array(),false) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::text('heading',null,array('class' => 'form-control','placeholder' =>'Enter news heading')) !!}
                            <label class="text-danger error" for="heading">{!! $errors->first('heading')!!}</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            {!! Form::label('description','Description') !!}
                        </div>
                        <div class="col-sm-8">
                            {!! Form::textarea('description',null,array('class' => 'form-control','placeholder' =>'Enter news description','cols' => '4','rows' => '4')) !!}
                            <label class="text-danger error" for="description">{!! $errors->first('description')!!}</label>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {!! Form::label('image','Image(s)') !!}
                        </div>
                        <div class="col-sm-8">
                            <div class="upload-btn-wrapper">
                                <button class="btn">Upload a file</button>
                                {!! Form::file('image[]', array('class' => 'image form-control-file','multiple' => 'multiple','id' => 'media_uploader','accept' => '.png,.jpg,.gif')) !!}
                            </div>
                            <label class="text-danger error" for="image">{!! $errors->first('image')!!}</label>
                            <div class="row mt-3" id="media_container">
                                @if(isset($data))
                                @php
                                $images=$data->newsMedia;
                                @endphp
                                @foreach(count($images) > 0 ? $images : array() as $image)
                                @include('news.imgPreview',['file_name' => $image->fileName,'image_id' => $image->id])
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {!! Form::label('date','Date') !!}
                        </div>
                        <div class="col-sm-2">
                            {!! Form::text('date',null,array('class' => 'form-control','id' => 'datepicker','autocomplete' => 'off')) !!}
                            <label class="text-danger error" for="date">{!! $errors->first('date')!!}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10 text-right">
                            <button class="btn btn-primary btn-wide mr5" id="saveNews" type="button">{!! (isset($data) ? "Update" : "Save") !!}</button>
                            <a href="{!! route('news') !!}" class="btn btn-primary  btn-wide mr5">Cancel</a>
                        </div>
                    </div>
                    <div class="clearfix"></div><br />
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@include('news.imgPreview')
@endsection
@section('script')
<script src="{{ asset('js/news.js') }}"></script>
@endsection