@extends('layout')
@section('content')
<div class="subpage">
        <!-- Banner -->
        <div class="banner latest_news_container">
                <img src="{{asset ('images/news/banner_news.jpg')}}" class="img-fluid" alt="newsbanner">
                <div class="w-100 smallcarouselcaption">
                        <h2 class="text-center">{{__('messages.latest_news')}}</h2>
                </div>
        </div>
        <!-- News -->
        <div class=" container mb-5">


                <div class="news">
                        @include("latestnewsajax",["news" => $data])
                </div>

                <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                                @if($data->count() != $data->total())
                                <div class="row mt-3 ">
                                        <div class="float-left col-md-2 previous_link_container mt-2">
                                                @if($data->onFirstPage())
                                                <i class="fas fa-chevron-left "></i>
                                                @else
                                                <a class="color-white navigation_link" href="{{$data->previousPageUrl()}}" id="previous_link">
                                                        <i class="fas fa-chevron-left "></i>
                                                </a>
                                                @endif
                                        </div>
                                        <div class="float-left col-md-8 links text-center">
                                                {{ $data->links('paginationlink') }}

                                        </div>
                                        <div class="float-right col-md-2 text-right next_link_container mt-2">
                                                @if($data->hasMorePages())
                                                <a class="color-white navigation_link" href="{{$data->nextPageUrl()}}" id="next_link">
                                                        <i class="fas fa-chevron-right"></i>
                                                </a>
                                                @else
                                                <i class="fas fa-chevron-right"></i>
                                                @endif
                                        </div>
                                </div>
                                @endif
                        </div>
                        <div class="col-md-4">
                        </div>
                </div>
        </div>
        @endsection