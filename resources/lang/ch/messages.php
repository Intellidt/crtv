<?php
return [
    'about_us'=>'关于我们',
    'business_introduction'=>'业务介绍',
    'business_banner_text'=>'诚邀天下有识之士共同打造媒体融合，影视工业，影视制作及文化创意产业的发展平台',
    'business1'=>'工业园',
    'business1_heading'=>'影视工业建设板块',
    'business1_detail'=>'围绕影视工业搭建影视工业园，建立中国影视工业化体系，推动中国影视产业工业化发展。
    中广国际传媒（北京）有限公司多年来一直积极探索中国影视工业化发展的创新之路，推动标准化体系建设，工业化流程再造和专业化岗位分工等方面做了有益的研究和尝试。并率先提出影视工业体系的概念。
    我们有理由相信，一个影视工业化时代的到来。
    构建由影视大数据系统为核心，并由其支撑起影视创意及IP工业系统，影视工业制作及管理系统，影视人力资源系统，全媒体终端服务系统，及影视金融服务系统等六大系统组成的影视工业体系。
    中广国际传媒将推动中国影视行业的工业革命！',
    'business2'=>'文旅板块',
    'business2_heading'=>'文化旅游创意板块',
    'business2_detail'=>'党的十九大把优先发展教育事业作为提高保障和改善民生水平的首要任务，并明确指出了素质教育的发展目标，因此教育与文化和旅游相结合，教育与产业相结合将有着巨大的发展空间和市场，为深化教育改革，提高旅游服务水平和促进地方发展开拓新的思路。
    十三五规划提出的创意文化，是在以往文化创意概念的基础上形成“文化+”的发展模式，文化与创意、旅游、教育相结合，创新发展思路，形成新的文化业态和小镇模式，打造创新、创业发展平台和新型城镇化有效载体。',
    'business2_detail_2'=>'六文合一',
    'business2_detail_3'=>'文旅+文创+文博+文教+文宣+文产',
    'business2_detail_4'=>'中广国际传媒拥有一支专业的文化创意策划团队，曾经成功的为首都国际机场文化国门建设，上海百乐门及邻水家文化小镇等项目积极提供专业策划。我们将六文合一的开创性策划理念并成功应用到项目中，有效服务并满足各个地方文化建设需求，推动习近平主席提出的新时代社会主义思想宣传。
    依托广电总局、中央电视台和文化旅游部，导入广播影视、视频制作、虚拟现实以及文化大数据等资源。创新文化业态，提升文化服务水平。以城市产业结构升级为契机，充分挖掘文创产业价值内涵，挖掘项目亮点，将优质的文化资源导入到文创项目中，助力城市文化繁荣，打造城市文化地标。
    拥有广电总局唯一一支产业基金，积极引导文化创意核心领域、新兴文化领域，服务地方文化产业发展，与地方政府一同打造为地方服务的文化金融服务平台，强化文创金融功能新支撑，探索文创金融结合新方式，提升文创投融资能力。
    作为广电国家队和文化央企，与地方政府有着深厚的合作关系，获得地方政府的合作信任和优惠政策。深度挖掘各地方优质的历史文化底蕴、民俗文化传统、特色景观资源，积极的为各地方政府打造优质的文旅产业项目、特色的产业生态作出巨大贡献。
    ',
    'business3'=>'影视剧',
    'business3_heading'=>'影视剧制作板块',
    'business3_detail'=>'致力于全面、专业的一站式影视内容制作商和出品商，构建起"影视剧投资，制作，发行"三位一体的商业生态链。
    依托大数据云平台，建设数字特效制作中心，引进虚拟合成及实景影棚、人体捕捉系统、渲染农场、顶级调色棚、数字美术特效工作站等，凸显分工专业化、流程细分化、生产分解化、人力资源高科技密集化和产业聚集化的特点，通过工业化制作体系对导演艺术要求的技术拆解，并在剧本阶段就由视觉和特效总监介入创作，后期前置，再由高科技劳动密集型制作团队承担后期制作，缩短影视制作周期，实现导演艺术追求，降低影片拍摄成本，真正实现影视工业化制作的聚集。发挥中广国际院线的发行优势，构建起“影视剧拍摄，制作，发行"三位一体的商业生态链。',
    'business4'=>'院线联盟',
    'business4_heading'=>'数字影院服务联盟板块',
    'business4_detail'=>'中广国际旗下中广国际数字电影院线（北京）有限公司是中影数字院线、华夏院线成立之后，国家广电总局系统内第三家院线公司，也是中央电视台拥有的唯一一条院线。 公司将立足现代传播体系，强化大局意识，打造主权院线；立足社会主义核心价值体系，强化政治意识，打造红色院线；立足构建公共文化服务体系，强化责任意识，打造惠民院线；立足探索文化产业发展新路，强化资本意识，打造合作院线。',
    'business5'=>'文化金融',
    'business5_heading'=>'文化金融服务板块',
    'business5_detail'=>'2014年5月，在广电总局的指导下，中广国际传媒联合全国16家省市电视台，共同发起中广传媒产业创新投资基金，并组建中广电媒投资基金管理无锡有限公司（简称中广电投）。
    作为文化产业的金融服务商，中广电投先后与甘肃、黑龙江、内蒙古、江苏、四川等省联合发起为各省文化产业量身定制的文化产业基金，共同搭建属于各省区的文化产业金融服务平台，并在影视产业园、文旅小镇和文化旅游创意等文化产业项目上进行投入，在文化产业与文化金融方面积累了丰富的经验。',
    'business6'=>'融媒体',
    'business6_heading'=>'媒体融合运营板块',
    'business6_detail'=>'中广国际传媒（北京）有限公司响应国家广电总局媒体融合的号召，积极推动县级融媒体中心和媒体融合的业务发展，创新运营媒体融合平台。
    融合权威媒体、传统媒体、新媒体、自媒体形成市场化的全媒体媒体融合平台，为客户提供全媒体服务。',
    'business6_task1_heading'=>'中国汽车研究中心 ',
    'business6_task1_detail'=>'发挥中广国际传媒影视拍摄、制作和发行的优势，拍摄策划和制作汽车主题记录片，讲述中国产汽车发展历史及中国汽车标准制定的发展历程，并在权威媒体和院线进行推广传播。    ',
    'business6_task2_heading'=>' 百乐门全媒体推广',
    'business6_task2_detail'=>'依托传统媒体、新媒体和自媒体等平台，以百乐门文化作为主体内容，根据不同媒体的表达方式，以不同艺术形式进行不同媒体的分发，使其在短时间内成为各媒体受众、各文化群层所能接受的文化品牌。',
    'latest_news'=>'最新资讯',
    'contact_us'=>'联系我们',
    'home_banner_text'=>'中广国际，文化产业的孵化器',
    'home_below_banner'=>'中国广播电视国际经济技术合作总公司（简称中广国际总公司）隶属于中国中央电视台、中国国际电视总公司，是中国广播电视行业的综合型企业。中广国际总公司以广播电影电视国家级涉外专业技术和管理队伍，贯彻“中•广•人•和”的企业文化理念，通过优质服务满足国内外客户的需求，为促进我国广播电影电视事业产业的发展做出积极贡献。',
    'service_1'=>'影视工业建设',
    'service_1_detail'=>'率先提出影视工业体系的概念，推动中国影视行业的工业革命',
    'service_2'=>'文化旅游创意',
    'service_2_detail'=>'文旅+文创+文博+文教+文宣+文产',
    'service_3'=>'影视剧制作',
    'service_3_detail'=>'构建起"影视剧投资，制作，发行"三位一体的商业生态链',
    'service_4'=>'数字影院服务联盟',
    'service_4_detail'=>'公司将立足现代传播体系，强化大局意识，打造主权院线',
    'service_5'=>'文化金融服务',
    'service_5_detail'=>'组建中广电媒投资基金管理无锡有限公司（简称中广电投）',
    'service_6'=>'媒体融合运营',
    'service_6_detail'=>'融合权威媒体、传统媒体、新媒体、自媒体形成市场化的全媒体媒体融合平台',
    'china_location'=>'中国北京朝阳区广渠东路1号中广国际综合楼三层',
    'canada_location'=>'6751 Westminster Hwy #200, Richmond, BC V6P4W5',
    'company_profile'=>'公司简介',
    'company_profile_heading'=>'中广国际传媒一直积极探索中国影视工业化发展的创新之路 (Replace)',
    'company_profile_detail1'=>'隶属于中国广播电视国际经济技术合作总公司，是国家广播电视总局直属的文化运营机构。公司国有出资人为中广国际广告公司（中广国际广告公司是中国广播电视国际经济技术合作总公司的全资子公司，是国家广播电视总局直属二级全民所有制企业），占有公司51%的股份，并由其派出运营团队，属于国有绝对控股和实际控制人的公司。',
    'company_profile_detail2'=>'公司成立以来，在广电总局的指导下，中广国际传媒积极推进主旋律影视作品的创作与宣发、媒体资源的融合与运营、影视工业体系的建设与管理、文化金融服务平台的打造与运作等四大业务版块的发展，在业内及我国文化产业领域逐渐形成较强的影响力，并参股组建了中广国际院线、中广国际咨询、中广文化传媒、中广电媒投资基金管理等公司，获得了院线经营、电影发行、基金管理等资质。',
    'our_team'=>'我们的队伍',
    'our_team_heading'=>'中广国际传媒一直积极探索中国影视工业化发展的创新之路 (Replace)',
    'member1_name'=>'王斌董事长 ',  
    'member1_detail'=>'毕业于首都师范大学历史系，高级项目管理师、国家注册拍卖师，历任北京电视台编辑、记者、主持人、制片人，中国康艺音像出版社副总编辑，北广传媒移动电视公司副总经理，北广传媒地铁电视公司副总经理。现任中广国际广告公司总经理、中广国际传媒（北京）有限公司、中广国际数字电影院线（北京）有限公司、中广电媒投资基金管理无锡有限公司董事长。',
    'member2_name'=>'许德平总经理',
    'member2_detail'=>'南京大学法学硕士，拥有律师、注册会计师资格。现任中广国际传媒（北京）有限公司总经理、中广电媒投资基金管理有限公司总经理、中广文影副董事长。15年以上投融资和投资银行经验，曾任职于中国大型律师事务所、会计师事务所和证券公司投资银行部，曾在国际大型投资机构和风投公司任职，并在实体经济中担任高级管理职位。并在国内法学和经济学核心期刊上发表多篇文章，其“论公司收购中的股东权益保护”一文在北京大学出版社的“经济科学”杂志发表，其“典权与外国不动产质权之比较研究”更为人大复印资料转载。',
    'member3_name'=>'段凯钟常务副总经理',
    'member3_detail'=>'重庆大学美视电影学院表演系本科，对外经济贸易大学企业管理在职研究生。现担任中广国际传媒（北京）公司常务副总经理，负责中国影视工业建设、中国第一个影视大数据板块业务以及公司围绕影视产业上下游的所有业务。在影视行业从业 18 年，作为主演、主创、监制及执行制片人，参与完成30 部以上影视剧的制作，拥有从演员、执行导演、监制、制片人到影视文化产业管理全产业链任职经历，曾担任中广影业、常务副总经理、中广国际院线副总经理等职。',
    'member4_name'=>'刘知林副总经理',
    'member4_detail'=>'湘潭大学历史系本科毕业，中国人民大学中共党史系中国革命史专业研究生毕业，从业经验30年，现任中广国际传媒（北京）有限公司副总经理，曾任北京青年报社人事处处长、北京广播影视集团人事部主任兼北京北广传媒集团公司总裁办公室主任、中广文化传媒无锡有限公司副总经理等职。',
    'member5_name'=>'杨轶鹏副总经理',
    'member5_detail'=>'中央党校本科，现担任中广国际传媒(北京)公司副总经理，中广国际数字电影院线（北京）有限公司副总经理，负责传媒及院线对外联络合作工作。曾任职北广传媒集团移动电视节目部主任。在传媒行业从业 24 年，行业资深人士。',
    'services_1_line'=>'中广国际传媒一直积极探索中国影视工业化发展的创新之路',
    'services_1_detail1'=>'为满足发展需要，服务发展大局，中广国际传媒一直积极探索中国影视工业化发展的创新之路，在推动标准化体系建设、工业化流程再造和专业化岗位分工等方面作了有益的研究和尝试，并率先提出影视工业体系的概念。公司构建了由影视大数据系统为核心，并由其支撑起影视创意及IP工业系统、影视工业制作及管理系统、影视人力资源服务系统、全媒体终端服务系统和影视金融服务系统等六大系统组成的影视工业体系。',
    'services_1_detail2'=>'以保证质量、控制成本为原则，形成科技为王，咨询优先，后期前置，数据支撑的影视工业制作体系，并打造影视工业聚集区。',
    'services_1_detail3'=>'以中广影视工业园为核心资源，整合区域性丰富的自然和历史文化资源，形成影视产业链的全覆盖。打造影视功能区，把影视产业作为地区文化产业发展的核心。',
    'services_2_line'=>'中广国际传媒一直积极探索中国影视工业化发展的创新之路 (Replace)',
    'services_2_detail'=>'依托混合云的形式，打造中国第一个影视云平台。平台将通过对相关影视大数据的采集、存储、加工、分析和应用，建设渲染农场，提高制作效率；通过对目标受众的大数据分析，倒推运行，推动创作双向化，实现从受众行为数据中挖掘受众意愿，再将受众意愿体现在作品中，开创“需求决定生产、观众决定创作”的工业化定制时代；促进影视剧生产的智能化、工业化、专业化、全面提升制作效率和质量；推动宣传营销由传统媒体向社交媒体、网络平台的转移，形成精准的营销模式；推动传统的影视链条和逻辑颠覆性转变，真正使大数据成为影视工业的原材料和工业体系的支撑点。',
    'services_3_line'=>'中广国际传媒一直积极探索中国影视工业化发展的创新之路 (Replace)',
    'services_3_detail'=>'中广国际已与中国科学院所属光电研究院、微电子所、集成电路创新院签署了合作协议，共同打造中广国际影视工业研究院。依托国家科研机构，整合尖端科技资源，推动影视文化与科技的进一步融合，加强影视科技供给，创新影视科技、研发影视设备、实现影视科研成果的市场转化和产业应用、探索制定中国影视工业化标准，并每年举办一届国际化的中广国际影视工业论坛。真正实现“科技为王”的影视工业发展理念。',
    'services_4_line'=>'中广国际传媒一直积极探索中国影视工业化发展的创新之路 (Replace)',
    'services_4_detail'=>'依托大数据云平台，建设数字特效制作中心，引进虚拟合成及实景影棚、人体捕捉系统、渲染农场、顶级调色棚、数字美术特效工作站等，凸显分工专业化、流程细分化、生产分解化、人力资源高科技密集化和产业聚集化的特点，通过工业化制作体系对导演艺术要求的技术拆解，并在剧本阶段就由视觉和特效总监介入创作，后期前置，再由高科技劳动密集型制作团队承担后期制作，缩短影视制作周期，实现导演艺术追求，降低影片拍摄成本，真正实现影视工业化制作的聚集。发挥中广国际院线的发行优势，构建起“影视剧拍摄，制作，发行”三位一体的商业生态链。',
    'services_5_line1'=>'以院线为基础的影院加盟',
    'services_5_line2'=>'以服务为粘性的影院联盟',
    'services_5_detail1_point'=>'通过服务提升资源价值',
    'services_5_detail1'=>'整合影院的空间资源，形成广告、连锁商业、活动营销等项目，通过目前冗余资源的运营实现收入，最终实现上述资源的价值。',
    'services_5_detail2_point'=>'提升影院的资产价值',
    'services_5_detail2'=>'通过对影院的服务、对影院排片的指导、甚至对影院的托管，提升影院票房和非票房的整体收入；通过服务平台的运作，为影院资产提供增值服务。',
    'services_5_detail3_point'=>'提升影院的资本价值',
    'services_5_detail3'=>'通过服务平台，提升运营效益的影院形成联盟之后将形成整体，其自身资本价值也具有了溢价空间，通过资本运作，实现交叉持股，最终创造影院的资本撬动效应。',
    'services_6_line'=>'唯一有资格制定中国汽车行业质量认证体系标准的机构',
    'services_6_detail1'=>'中广国际传媒与中国汽车技术研究中心合作，发挥中广国际传媒自身的媒体资源优势，以全媒体营销的形式对中国汽车标准的强制性标准和推荐性标准进行推广与传播，打造汽车标准的品牌IP，从而奠定标准在百姓心中的权威和依赖，引导并指导汽车行业执行标准。',
    'services_6_detail2'=>'在权威媒体、传统媒体、新媒体及自媒体等全媒体进行有策略、有规划、延续性的输出优质内容。发挥中广国际传媒影视拍摄、制作和发行的优势，拍摄策划和制作汽车主题记录片，讲述中国产汽车发展历史及中国汽车标准制定的发展历程，并在权威媒体和院线进行推广传播。',
    'icon_1_name'=>'商业运营',
    'icon_2_name'=>'粉丝运作 ',
    'icon_3_name'=>'培训人才',
    'icon_4_name'=>'物联网服务',
    'icon_5_name'=>'供应链金融服务',
    'icon_6_name'=>'活动营销 ',
    'icon_7_name'=>'社交电商',
    'icon_8_name'=>'卫生产品 ',
    'icon_9_name'=>'版权发行',
    'icon_10_name'=>'广告招商'
];
?>